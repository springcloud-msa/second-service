package com.example.secondservice.controller;

import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jakarta.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/second-service")
public class SecondController {
	Environment env;
    
    //DI(Dependency Injection) - Constructor args
    public SecondController(Environment env) {
		this.env = env;
	}

	@GetMapping("/health-check")
	public String status(HttpServletRequest request) {
		return String.format("%s Connected!! by port %s "
								, env.getProperty("spring.application.name") 
								, request.getServerPort() );
	}
}
